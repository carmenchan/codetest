## Code Test Instruction

The challenge is to convert code-test.psd into HTML. Our recommendation is to not spend too much time trying to get it perfect, but demonstrate your skills through well structured code and industry standard conventions. This means code formatting, comments and readable variable names. This will be our primary assessment focus. 

Included is the data file, images and PSD.

Requirements:
* Build with MV* framework 
* Read data from JSON
* Responsive Layout
* Build dropdown filters from data
* Hide and show data depending on the dropdown filters

We encourage you not to use pre-made HTML boilerplates, and to only use what is needed for this task. We want to see what you can do.


## Code Test (Documentation)

### Prerequisites
- Install Node.js and NPM (https://nodejs.org/en/)

### Installation 
1. Clone the project from this repo
2. At the terminal, run
```
  npm install
```

### Run the project
At the terminal, run
```
  npm start
```

#### Remarks
* This application is done by ReactJS. 
* The project is initially created by using facebook create react app (https://github.com/facebook/create-react-app). 
* This application is only used to demostrate the requirements stated above. In the real world, the application should be created in better way. E.g. start with better boilerplate, use of webpack, redux, place font-size in _variables.scss, create core.scss, grid.scss, use mixin, add print stylesheet, include test, create min version for deployment, web accessibilty  etc.

If you have any questions, please send email to Carmen (to.carmenchan@gmail.com).
