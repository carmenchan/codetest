import React, { Component } from 'react';
import { Col, Clearfix } from 'react-bootstrap';

/*Show single product*/
class ProductItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
  }
  render() {
    var product = this.state.product;

    return (
      <Col md={3} sm={6} className="product__tile">
        <div className="product__image text-center">
          <img alt={product.productName} src={require("../../assets/images/"+product.productImage)} />
        </div>
        <Clearfix />
        <div className="product__offer clearfix">
          {
            product.isSale && <div className="offer offer--sale">
             <span>Sale</span>
            </div>
          }
          {
            product.isExclusive && <div className="offer offer--exclusive">
             <span>Exclusive</span>
          </div>
          }
        </div>
        <Clearfix />
        <div className="product__info">
          <div className="product__name">
            {product.productName}
          </div>
          <div className="product__price">
            {product.price}
          </div>
        </div>
      </Col>
    );
  }
}

export default ProductItem;
