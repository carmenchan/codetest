import React, { Component } from 'react';
import { Row } from 'react-bootstrap';
import ProductItem from '../ProductItem/ProductItem';

/* Loop all products and pass data to product item component */
class ProductList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
  }

  // update state when filter products
  componentWillReceiveProps(nextProps) {
    this.setState({ productData: nextProps.productData });   
  }
  render() {
    const productHtml = this.state.productData.map(function(product, key) {
      return <ProductItem product={product} key={key} />
    });

    return (
      <Row className="product__list">
        {productHtml}
      </Row>
    );
  }
}

export default ProductList;