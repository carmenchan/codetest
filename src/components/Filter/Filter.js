import React, { Component } from 'react';
import { FormControl, FormGroup } from 'react-bootstrap';

class Filter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props
    }
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange = (event) => {
    // Display products with selected size 
    this.state.filterProduct(event);  
  }

  render() {
    const dropdownFilterItem = this.state.filterSizes.map(function(size, key) {
      return <option value={size} key={"menu-"+key}>{size}</option>
    })
    return (
      <FormGroup controlId="filter" className="filter">
        <FormControl componentClass="select" placeholder="Filter by size" onChange={this.handleChange}>
          <option value="all">Filter by size</option>
          {dropdownFilterItem }
        </FormControl>
      </FormGroup>
    );
  }
}

export default Filter;