import React, { Component } from 'react';
import { Grid, Col, Row } from 'react-bootstrap';
import productData from './data/products.json'; 
import ProductList from './components/ProductList/ProductList';
import Filter from './components/Filter/Filter';

import './styles/styles.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      productData: productData,   
      filterProductData: productData,
      selectedSize: ''
    };
    this.filterProduct = this.filterProduct.bind(this);
  }

  componentWillMount() {
    var filterSizes = []
    var sizeArraySort = ["XS", "S", "M", "L", "XL"]   //Sizes by order
    
    // eslint-disable-next-line
    productData.map(function(product,key) {

      // eslint-disable-next-line
      product["size"].map(function(size) {
        if (filterSizes.indexOf(size) === -1) {
          filterSizes.push(size)
        }
      })

    })

    filterSizes.sort(function(a, b){  
      return sizeArraySort.indexOf(a) - sizeArraySort.indexOf(b);  //sort size in order
    });

    this.setState({filterSizes: filterSizes});
  }

  filterProduct = (evt) => {
    // Display all products
    if (evt.target.value === 'all') {
      this.setState({selectedSize: evt.target.value, filterProductData: this.state.productData})    //show all products if selected value is filter by size
      return;
    }

    var filterProductData = [];

    // eslint-disable-next-line
    this.state.productData.map(function(product, key) {
      if (product.size.indexOf(evt.target.value) === -1) {
        filterProductData.push(product);
      }
    })

    // Update state for filtered product list 
    this.setState({selectedSize: evt.target.value, filterProductData: filterProductData})  
  }
  render() {
    return (
      <Grid>
        <Row className="product__list-header">
          <Col md={9} sm={9}>
            <h2 className="product__category">Women's tops</h2>
          </Col>
          <Col md={3} sm={3}>
            <Filter filterProduct={this.filterProduct} filterSizes={this.state.filterSizes}/>
          </Col>
        </Row>
        <ProductList productData={this.state.filterProductData} />
      </Grid>
    );
  }
}

export default App;